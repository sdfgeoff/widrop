# What is the Widrop?
The Widrop is a fusion of geocaching and twitter. It creates a small
wifi network and webpage that people can leave short messages on, similar
to writing your name in the log book at a geocache.

The hardware is intended to be as cheap and simple as possible, and, 
if possible, solar powered. I am aiming for a per-unit cost of under $10.

Intended Hardware:

 - [ESP-12 Module](http://www.electrodragon.com/product/esp-12f-esp8266-wifi-board/)(~$3)
 - [Solar-powered garden light](https://www.mitre10.co.nz/shop/orbit-solar-path-light-h-340mm/p/269573) I can find (~$5 from local hardware store)
 - [Boost Converter](http://www.aliexpress.com/item/Ultra-small-DC-DC-0-8-3-3V-to-DC-3-3V-Step-UP-Boost-PFM/32253709053.html) because most solar lights are a single nimh battery (~$2)
 
 
# Current Status
Currently effort is going into the development of the software for the ESP.

Functioning:

 - DNS redirection
 - Webpage hosting
 - Displaying submitted messages
 - Submitting messages
 
Todo:
 
 - Make the webpage look prettier
 - Figure out why binary files are being corrupted
 - Figure out mime types to squelsh browser warnings
 - Figure out why it works perfectly to desktop browsers but timeouts to mobile ones

# Installation/creating your own
When I have some more hardware, I'll start a hackaday project. For now, to load the
software, you need to:

1. Download/clone this repo
2. Flash micropython onto the ESP module (esptool and a working firmware are in the tools folder)
3. Run upload.py to put the widrop code onto the ESP module
4. Restart the ESP module

Now you should see a new wifi network called 'WiDrop'

# History/Inspiration
Several years ago, some people had the idea of embedding USB drives
into brick walls, and calling them USB dead drops. People could plug their
laptop into them and exchange files with whoever had been there before.
A bit later, the 'widrop' was tried, with a wifi router. But setting up and
maintaining these things takes time and effort. So instead of going for
file storage, I decided to go for small messages. It meant that the whole thing can
fit into the 4mb of flash on an ESP-12 module, the cheapest wifi module around.