#!/usr/bin/python3
'''
Uploads files in a source directory to the ESP8266 over UART.
'''
import os

from tools import pyboard

SRC_FOLDER = './src'

USB_PORT = '/dev/ttyUSB0'
BAUD = 115200


def upload():
    '''Uploads the contents of the SRC_FOLDER directory to the ESP
    module'''
    for file_to_upload in os.listdir(SRC_FOLDER):
        print("Uploading", file_to_upload, end='')
        file_path = os.path.join(SRC_FOLDER, file_to_upload)
        file_write_str = generate_file_str(file_path)
        run_text(file_write_str)
        print(" ... Done")
    print("Finished Uploading to ESP")


def generate_file_str(file_path):
    '''Reads a file and converts it into a string that can be run
    on a python interpreter to recreate the file'''
    file_contents = open(file_path, 'rb').read()
    file_name = os.path.basename(file_path)
    file_str = '''f = open("{}", "wb");
f.write("""{}""");
f.close()'''.format(file_name, repr(file_contents)[2:-1])
    return file_str


def run_text(text):
    '''Executes a string on the ESP module uysing pyboard'''
    pyb = pyboard.Pyboard(USB_PORT, BAUD, 'micro', 'python')
    pyb.enter_raw_repl()
    output = pyb.exec_(text)
    pyboard.stdout_write_bytes(output)
    pyb.exit_raw_repl()
    pyb.close()


if __name__ == "__main__":
    upload()
