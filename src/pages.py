'''
Handles anything to do with the HTML pages
'''
import os
import time
import machine

MAX_MESSAGES = 20
MAX_LENGTH = 100*4/3  # The *4/3 is because base64 takes longer
PAGE_404 = 'redirect.html'
MESSAGE_REPLACE_STR = b"['VGhpcyBpcyBhIHRlc3QsIGFuZCBxdWl0ZSBhIGxvbmcgb25l']"
STATS_REPLACE_STR = b"{'Node ID':0, 'Uptime':0}"


def add_message(query):
    '''Adds a message to the message log'''
    for char in query:
        if not (char.isalpha() or char.isdigit() or char == '=') \
                or len(query) > MAX_LENGTH:
            print("Rejecting Message", query)
            return
    if 'messages.txt' not in os.listdir():
        messages = open('messages.txt', 'w')
        messages.write('\n')
        messages.close()
    messages = open('messages.txt')
    entries = messages.readlines()
    messages.close()
    entries = [query] + entries
    if len(entries) > MAX_MESSAGES:
        entries = entries[:MAX_MESSAGES]
    entries_str = '\n'.join([e.strip() for e in entries])
    print(entries_str)
    messages = open('messages.txt', 'w')
    messages.write(entries_str)
    messages.close()


def generate_visitor_page():
    '''Generates the home page including visitors book'''
    print('Generating visitor page')
    visitor_js = open('visitor.js', 'rb')
    raw = visitor_js.read()
    visitor_js.close()
    entries = open('messages.txt', 'rb').read().replace(b'\n', b'","')
    visitors_string = b'["'+entries[:-3]+b'"]'
    raw = raw.replace(MESSAGE_REPLACE_STR, visitors_string)
    stats_string = generate_stats_str()
    raw = raw.replace(STATS_REPLACE_STR, bytes(stats_string, 'utf-8'))
    return raw


def generate_stats_str():
    '''Creates a string containing vital statistics for the node'''
    stats_dict = dict()
    stats_dict['Node ID'] = '-'.join(["%02X" % o for o in machine.unique_id()])
    stats_dict['Uptime'] = str(time.time()) + " Seconds"
    voltage = machine.ADC(0).read() * 5.6394 / 1000
    stats_dict['Battery'] = "{:.2f} Volts".format(voltage)
    return repr(stats_dict)


def parse_page_request(page_request):
    '''Converts the page request into a page string'''
    page_request_str = page_request.decode('UTF-8')
    if page_request_str.find('?') != -1:
        page_request_str, query = page_request_str.split('?')
        if page_request_str == 'submit.html':
            add_message(query)

    print('Client requested page:', page_request_str)
    if page_request_str in os.listdir():
        if page_request_str == 'visitor.js':
            return generate_visitor_page()
        else:
            raw = open(page_request_str, 'rb')
            text = raw.read()
            raw.close()
            return text
    else:
        return open(PAGE_404).read()
