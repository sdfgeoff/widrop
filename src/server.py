'''A simple HTTP and DNS server'''

import socket
import time
import pages

HTTP_ADDRESS = ('0.0.0.0', 80)
DNS_ADDRESS = ('', 53)
PRE_READ_DELAY = 200  # Time to wait before expecting client to sent something
BUFFER_SEND_TIME = 100  # Time to wait between sending bits of long messages
CLIENT_TIMEOUT = 1000


def get_own_ip_bytes(wifi):
	'''Returns a list of the raw bytes that make up the ESP's IP Address'''
	own_ip = wifi.ifconfig()[0]
	own_ip = [int(i) for i in own_ip.split('.')]
	return bytearray(own_ip)


def get_page_request(client):
	'''If the client sends a HTTP header, return the page requested'''
	try:
		time.sleep_ms(PRE_READ_DELAY)
		raw = client.read(1024)
	except OSError:
		print("Client Timed Out")
		return None
	if raw is not None:
		lines = raw.split(b'\n')
		for line in lines:
			if line.find(b'GET') != -1:
				page_request = line.split(b' ')[1]
				return page_request


def serve_page(http_socket):
	'''A dumb http server that can serve multiple pages'''
	try:
		client, addr = http_socket.accept()
	except OSError:
		return
	print('Client connected')
	page_request = get_page_request(client)

	if page_request is not None:
		page_str = pages.parse_page_request(page_request[1:])
		try:
			client.settimeout(CLIENT_TIMEOUT)
			while len(page_str) > 0:
				sent = client.send(page_str)
				page_str = page_str[sent:]
				time.sleep_ms(BUFFER_SEND_TIME)
			client.settimeout(0)
		except OSError:
			print(addr, 'Client timed out')

	client.close()


def serve_dns(wifi, dns_socket):
	'''A DNS-redirector so all DNS lookups get forwarded to the
	ESP's ip address'''
	try:
		data, addr = dns_socket.recvfrom(1024)
	except OSError:
		return
	print('Client requested dns')

	packet = generate_dns_response(data, wifi)
	dns_socket.sendto(packet, addr)


def generate_dns_response(data, wifi):
	'''Generates a DNS packet with the ESP's IP address'''
	own_ip = get_own_ip_bytes(wifi)

	packet = b''
	packet += data[:2] + b'\x81\x80'
	packet += data[4:6] + data[4:6] + b'\x00\x00\x00\x00'
	packet += data[12:]
	packet += b'\xc0\x0c'
	packet += b'\x00\x01\x00\x01\x00\x00\x00\x3c\x00\x04'
	packet += own_ip
	return packet


def create_http_socket():
	'''Creates a TCP socket to be used for servicing HTTP requests'''
	http_socket = socket.socket()
	http_socket.bind(HTTP_ADDRESS)
	http_socket.listen(1)
	http_socket.settimeout(0)
	return http_socket


def create_dns_socket():
	'''Creates a UDP socket to be used as a DNS server'''
	dns_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
	dns_socket.bind(DNS_ADDRESS)
	dns_socket.setblocking(0)
	return dns_socket


def start(wifi):
	'''Runs the server'''
	http_socket = create_http_socket()
	dns_socket = create_dns_socket()

	print("Starting Server")
	while True:
		serve_dns(wifi, dns_socket)
		serve_page(http_socket)
