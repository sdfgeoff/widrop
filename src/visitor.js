max_length = 100
messages = ['VGhpcyBpcyBhIHRlc3QsIGFuZCBxdWl0ZSBhIGxvbmcgb25l'] //Magic!
statistics = {'Node ID':0, 'Uptime':0} //Magic #2
default_message = "Type Here"

function remaining_counter(){
	text = document.getElementById('message').value
	counter = document.getElementById('counter')
	rem = max_length - text.length
	counter.innerHTML = '<small>Remaining: '+rem.toString()+'</small>'
	submit = document.getElementById('submit')
	submit.disabled = (text == '' || text == default_message || rem < 0)
}

function clear_box(){
	message = document.getElementById('message')
	if (message.value == default_message){
		message.value = ''
                        remaining_counter()
	}
}

function send(){
	message = document.getElementById('message')
	text = message.value.slice(0, max_length)
	url = 'submit.html?'+ btoa(text)
	window.location = url
}

function populate_visitors_book(){
	book = document.getElementById('visitors_book')
	for (i in messages){
                try{
                        text = atob(messages[i])
                } catch(err) {continue}
                para = document.createElement("div")
                para.appendChild(document.createTextNode(text))
                para.className = 'visitor_message' 
                book.appendChild(para)
	}
}

function populate_statistics(){
        stat_div = document.getElementById('statistics')
        table = document.createElement('table')
        table.className = 'stats'
        for (stat in statistics){
                stat_value = statistics[stat]
                row = table.insertRow()
                cell = row.insertCell()
                cell.innerHTML = stat
                cell2 = row.insertCell()
                cell2.innerHTML = stat_value
        }
        stat_div.appendChild(table)

}
window.onload = function(){
        populate_visitors_book()
        populate_statistics()
	message = document.getElementById('message')
	message.maxLength = max_length
	message.oninput = remaining_counter
	message.onfocus = clear_box
	message.value = default_message
	remaining_counter()
	submit = document.getElementById('submit')
	submit.onclick = send
}
