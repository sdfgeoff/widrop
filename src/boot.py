import network
import esp

NETWORK_NAME = 'WiDrop'

# Create Wifi network:
WIFI = network.WLAN(network.AP_IF)
WIFI.active(True)
WIFI.config(essid=NETWORK_NAME, authmode=0)

# Disable ESP gumpf
esp.osdebug(None)

# Run main script
import server
server.start(WIFI)
